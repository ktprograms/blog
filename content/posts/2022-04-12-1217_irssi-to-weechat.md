+++
title = 'Switching from Irssi to Weechat'
date = 2022-04-12T12:17:22+08:00
draft = false
+++

A few months ago, when I started using IRC, I decided to use the [Irssi](https://irssi.org) client.

I found Irssi to be adequate for my usecase in all but one way: it doesn't have an unread marker/indicator. I recently decided to switch to [Weechat](https://weechat.org), initially expecting that having an unread indicator would be the only useful addition.

As it turns out, my expectations were wrong. Weechat is _amazing_! It has support for logging, built in mouse support and a really powerful interactive settings manager ([fset](https://weechat.org/files/doc/devel/weechat_user.en.html#fset)). There's also a nicklist that shows all the users in a channel, as well as a buflist for visually viewing all buffers.

All in all, Weechat feels much more polished and advanced than Irssi, and I think switching was well worth the ~2 hours it took me to configure Weechat.
