+++
title = 'TIL: macOS Serial Console Login'
date = 2023-01-24T10:04:16+08:00
tags = [ 'Today I Learned', 'macOS', 'Virtual Machines' ]
+++

When using [Virtualization.framework](https://developer.apple.com/documentation/virtualization/) to virtualize macOS VMs, I occasionally have no use for the GUI, and even get frustrated at the slowness (especially when scrolling in Terminal.app). Since Virtualization.framework supports adding a VirtIO serial device, I decided to try doing that, but when I booted the VM, nothing showed up.

After some searching, I found that I needed to modify `/etc/gettytab` and add a file to `LaunchDaemons` to start a `getty` on the serial console from [this answer](https://superuser.com/a/1062709). However, in order to get a proper login banner (from the `default` entry) as well as a 256 color terminal, I used this `gettytab` line instead:

```text
virtio:\
        :np:sp#115200:tt=xterm-256color:
```

I then added a `serialconsole.plist` file to `/Library/LaunchDaemons` similar to the one in the answer, but changed it to reference the `virtio` `gettytab` entry and `cu.virtio` serial device:

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs//
PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>KeepAlive</key>
    <true/>
    <key>Label</key>
    <string>serialconsole</string>
    <key>ProgramArguments</key>
    <array>
        <string>/usr/libexec/getty</string>
        <string>virtio</string>
        <string>cu.virtio</string>
    </array>
</dict>
</plist>
```

After rebooting, I was able to log in from the serial console. At this point, I was able to remove the GUI display from the VM, and everything continued to work properly.
