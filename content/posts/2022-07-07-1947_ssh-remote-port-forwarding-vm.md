+++
title = 'TIL: SSH Remote Port Forwarding for Virtual Machines'
date = 2022-07-07T19:47:49+08:00
draft = false
tags = [ 'Today I Learned' ]
+++

When using [virt-manager](https://virt-manager.org/) to create virtual machines, by default it uses a NAT network which can't be accessed from outside the host machine. It is possible to use bridge interfaces for that, but it seems that bridging is not supported on `wlan0` on my Thinkpad.

I decided to try and use SSH Port Forwarding to make a server running on the VM be accessible from the host's IP address. At first, I tried local port forwarding from the host, but that only allowed the server to be accessible from the host's `localhost`. Eventually, I found [this serverfault post](https://serverfault.com/questions/285616/how-to-allow-remote-connections-from-non-localhost-clients-with-ssh-remote-port), which was describing exactly what I wanted to achieve, and it turns out that remote port forwarding was what I needed. For example, to forward port 22 (SSH) on the VM to port 2222 on the host, I needed this command:
```sh
ssh -R <host-ip-on-nat>:2222:localhost:22 -l <user-on-host> <host-ip-on-nat>
```
The `<host-ip-on-nat>` can be read by running `ip a` on the host and getting the IP address for an interface named like `virbr0`.

I had one last problem, though, that the warning `Warning: remote port forwarding failed for listen port <port>` was showing up when running the remote port forwarding on the VM. I found [this answer](https://superuser.com/questions/1194105/ssh-troubleshooting-remote-port-forwarding-failed-for-listen-port-errors/1194856#1194856) that said I needed to set these options in my `sshd_config`:
```
AllowTcpForwarding yes
GatewayPorts yes
```

After then restarting the `sshd` service, I was able to make remote port forwarding work, and could SSH into the VM from outside the host with this:
```sh
ssh <user-on-vm>@<host-ip> -p 2222
```
